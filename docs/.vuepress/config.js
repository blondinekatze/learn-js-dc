const { description } = require('../../package')

module.exports = {
    title: 'Обучение DC',
    base: '/learn-js-dc/',
    dest: 'public',
    description: description,

    head: [
        ['meta', { name: 'theme-color', content: '#3eaf7c' }],
        ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
    ],

    themeConfig: {
        repo: '',
        editLinks: false,
        docsDir: '',
        editLinkText: '',
        lastUpdated: false,
        nav: [
            {
                text: 'JS',
                link: '/lessons/js/',
            },
            {
                text: 'Vue',
                link: '/lessons/vue/',
            }
        ],
        sidebar: {
            '/lessons/js/': [
                {
                    title: 'JS',
                    collapsable: false,
                    sidebarDepth: 1,
                    children: [
                        'lesson1',
                      /*  'lesson2',
                        'lesson3',
                        'lesson4',
                        'lesson4-1',
                        'lesson5',
                        'lesson5-1',*/
                        /*'lesson6',*/
                    ]
                }
            ],
            '/lessons/vue/': [
                {
                    title: 'Vue',
                    collapsable: false,
                    sidebarDepth: 1,
                    children: [
                        'lesson1',
                    ]
                }
            ]
        }
    },

    plugins: [
        '@vuepress/plugin-medium-zoom',
    ]
}
