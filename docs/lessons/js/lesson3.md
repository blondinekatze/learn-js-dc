# Урок №3

## Задание
Реализовать следующие моменты:
- Первый взнос - динамическая замена максимального значения в зависимости от цены квартиры.
- Динамические изменения платежа и суммы кредита в зависимости от типа квартиры и значений range-слайдеров

## Обязательно проверить следующее:
- Если процент = 0, то платеж должен рассчитываться по формуле = сумма кредита / кол-во месяцев
- Если цена квартиры становится меньше, чем выбрано в первоначальном взносе, то не должно быть отрицательных значений

## Теоретическая часть
### Слайдеры
Инициализацию слайдеров переносим в наш класс. Рекомендую сделать под это отдельную функцию (например, initSliders() ).
Также в конструкторе объявляем новый объект this.sliders. Каждое значение данного объекта будет отвечать за соответствующий
проинициализированный слайдер. Далее будет показано для чего нам это необходимо

```js
constructor() {
  this.sliders = {
    amount: '',
    years: '',
    percent: ''
  }
}

init() {
  //наш код
  this.initSliders()
}

initSliders() {
  //код инциализации слайдеров
}

```

В функции initSliders() делаем инициализацию всех слайдеров, где указываем что должно происходить при изменении onChange
и обновлении данных слайдера onUpdate
```js
$slider.ionRangeSlider({
  onChange: (data) => {
    // здесь указываем что изменяются данные в объекте this.fields соответствующего параметра, используем data.from
    // здесь запускаем перерасчет кредитной цены и месячного платежа
  },
  onUpdate: (data) => {
    // здесь указываем что изменяются данные в объекте this.fields соответствующего параметра, используем data.from
    // здесь запускаем перерасчет кредитной цены и месячного платежа
  }
})

this.sliders[years] = $range.data('ionRangeSlider') //записываем в наш объект проинициализированный слайдер для возможности
//его обновления далее
```

### Обработка клика по типам квартир
В фунцию bindEvents() записываем обработку клика по типам квартир. Данная функция группирует все типовые действия, что 
будут происходить в нашем классе. Не забываем bindEvents() вызывать в init()
При клике по блоку типа квартиры получаем значение data-price и записываем его в this.fields.price.
И обновляем максимальное значение слайдера первоначального взноса
```js
this.sliders.amount.update({
  max: this.fields.price
});
this.paymentСalculate() // не забываем про вызов функции перерасчета, чтобы обновить наши данные
```
В функции init() таким же образом надо обновить максимальное значение в слайдере первоначального платежа после получения
цены квартиры
