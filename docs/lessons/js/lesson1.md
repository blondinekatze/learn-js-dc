# Урок №1

## Верстка блоков
[https://yadi.sk/i/JKMQa6rxMlDCng](https://yadi.sk/i/JKMQa6rxMlDCng)

Дизайн идет по желанию, на скрине приведен приблизительный пример

Блоки:
- типы квартир (с ценами в data-атрибуте)
```html
<div class="radio-group__item js-type-items" data-price="1800000">Студия</div>
```
- первый взнос - range-слайдер с ценами 
- срок кредита - range-слайдер с кол-вом лет
- первый взнос - range-слайдер с процентной ставкой

Используемый плагин [http://ionden.com/a/plugins/ion.rangeSlider/](http://ionden.com/a/plugins/ion.rangeSlider/)
```html
<!--Пример одного слайдера, подпись указываем в data-атрибуте -->
<input class="js-range-slider" 
   type="text"
   value="300000"
   data-min="0"
   data-max="5000000"
   data-postfix=" ₽"
/>
```

```js
//Пример иниализации всех слайдеров
$('.js-range-slider').each(function () {
  $(this).ionRangeSlider({
    grid: false
  });
})
```
- итоговый блок с полями
    - ежемесячный платеж
    - сумма кредита

Для вывода цифровых значений платежа и суммы можно использовать следующую запись в js:
```html
<!--Для того, чтобы перезаписывать цену/платеж рекоменую обернуть саму цифру в отдельный
span и присвоить блоку js-класс или data-атрибут -->
<div class="calc-value">
  <span class="js-price-month">0</span> ₽
</div>
```
```js
new Intl.NumberFormat('ru-RU').format(creditPrice)
```

Это позволяет форматировать числа, чтобы были автоматические пробелы после каждой третьей цифры. Никакие плагины подключать не надо
